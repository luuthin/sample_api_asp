﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using SAMPLE_API.Models.RequestDTO;
using SAMPLE_API.Models.ResponseDTO;
using SAMPLE_API.Common;
using SAMPLE_API.Utils;

using SAMPLE_API.Business.User;
using System.Web;
using Newtonsoft.Json;
using static SAMPLE_API.Utils.JwtAuthProvider;
using static SAMPLE_API.Utils.Logger;
using System.Web.Http.Cors;

namespace SAMPLE_API.Controllers
{
    public class UserController : ApiController
    {
        // USER LOGIN
        [HttpPost]
        [Route("api/login")]
        public object Post([FromBody] UserRequestDTO user)
        {
            Logger.Info(Request.RequestUri + "  ||  Method: " + Request.Method, Request.Headers.ToString(), JsonConvert.SerializeObject(user), Level.INFO);

            ResponseDTO RES = new ResponseDTO();
            ErrorDTO error = new ErrorDTO();
            LoginResponseDTO LoginRes = new LoginResponseDTO();

            string email = user.email != null ? user.email : "";
            string password = user.password != null ? user.password : "";
            string timestamp = user.timestamp != null ? user.timestamp : "";
            string signature = user.signature != null ? user.signature : "";

            if (email == "" || password == "" || timestamp == "" || signature == "")
            {
                error.code = Constans.INVALID_DATA_CODE;
                error.message = Constans.INVALID_DATA_MSG;

                RES.error = error;

                Logger.Info(user.email + " || " + error.message, Level.INFO);
            }
            else
            {
                bool checkUserLogin = LoginBUS.CheckUserLogin(user.email, user.password);

                if (LoginBUS.CheckSignature(user.timestamp, user.signature) == null)
                {
                    if (checkUserLogin)
                    {
                        string token = GenerateToken(email, password);

                        LoginRes.id = "123";
                        LoginRes.token = token;
                        LoginRes.email = user.email;


                        RES.data = LoginRes;
                    }
                    else
                    {
                        error.code = Constans.LOGIN_FAILED_CODE;
                        error.message = Constans.LOGIN_FAILED_MSG;

                        RES.error = error;

                        Logger.Info(user.email + " || " + error.message, Level.INFO);
                    }
                }
                else
                {
                    RES.error = LoginBUS.CheckSignature(user.timestamp, user.signature);
                   
                }
            }

            return RES;
        }

        // CREATE SUB USER
        [HttpPost]
        [Route("api/users/create-sub-user")]
        public object CreateNewUser([FromBody]UserRequestDTO userData)
        {
            string token = Request.Headers.Authorization == null ? "" : Request.Headers.Authorization.ToString();
            Logger.Info(Request.RequestUri + "  ||  Method: " + Request.Method, Request.Headers.ToString(), JsonConvert.SerializeObject(userData), Level.INFO);

            ResponseDTO RES = new ResponseDTO();
            ErrorDTO error_sig = new ErrorDTO();
            ErrorDTO error = new ErrorDTO();
            UserResponseDTO UserRes = new UserResponseDTO();

            error_sig = LoginBUS.CheckSignature(userData.timestamp, userData.signature);

            // CHECK token
            if (ValidateToken(token))
            {

                if(error_sig == null)
                {
                    string email = userData.email != null ? userData.email : "";
                    string password = userData.password != null ? userData.password : "";
                    string timestamp = userData.timestamp != null ? userData.timestamp : "";
                    string signature = userData.signature != null ? userData.signature : "";

                    if (email == "" || password == "" || timestamp == "" || signature == "")
                    {
                        error.code = Constans.INVALID_DATA_CODE;
                        error.message = Constans.INVALID_DATA_MSG;
                        RES.error = error;
                    }
                    else
                    {
                        if (UserBUS.CheckExistUser(userData.email))
                        {
                            error.code = Constans.USER_EXISTED_CODE;
                            error.message = Constans.USER_EXISTED_MSG;
                            RES.error = error;
                        }
                        else
                        {
                            UserRes = UserBUS.CreateUser(userData.email, userData.password, userData.timestamp, userData.signature);

                            RES.data = UserRes;
                        }
                    }
                }else
                {
                    error = error_sig;
                    RES.error = error;
                }
                

            }else
            {
                error.code = Constans.NOT_AUTHEN_CODE;
                error.message = Constans.NOT_AUTHEN_MSG;
                RES.error = error;
            }


            return RES;
        }
        
    }
}

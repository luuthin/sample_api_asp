﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAMPLE_API.Models.RequestDTO
{
    public class UserRequestDTO
    {
        public string email { get; set; }
        public string password { get; set; }
        public string timestamp { get; set; }
        public string signature { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAMPLE_API.Utils
{
    public class Constans
    {
        public const string NOT_AUTHEN_CODE = "NOT_AUTHEN";
        public const string LOGIN_FAILED_CODE = "LOGIN_FAILED";
        public const string INVALID_DATA_CODE = "INVALID_DATA";
        public const string USER_EXISTED_CODE = "USER_EXISTED";

        public const string NOT_AUTHEN_MSG = "Thông tin xác thực user không đúng";
        public const string LOGIN_FAILED_MSG = "Email hoặc password không đúng";
        public const string INVALID_DATA_MSG = "Dữ liệu không hợp lệ";
        public const string USER_EXISTED_MSG = " Người dùng đã tồn tại";
    }
}
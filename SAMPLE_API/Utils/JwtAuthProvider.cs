﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SAMPLE_API.Utils
{
    public class JwtAuthProvider  
    {
        private static string Secret = Signature.GenerateSecret("abc1234");

        public static string GenerateToken(string email, string password)
        {
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Email, email)
                    }),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(Secret);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }

            catch (Exception)
            {
                return null;
            }
        }

        //private static bool ValidateToken(string token, out string email)
        public static bool ValidateToken(string token)
        {
            string email = null;
            var simplePrinciple = GetPrincipal(token);
            if(simplePrinciple != null)
            {
                var identity = simplePrinciple.Identity as ClaimsIdentity;
                if (identity == null) return false;
                if (!identity.IsAuthenticated) return false;
                var emailClaim = identity.FindFirst(ClaimTypes.Email);
                email = emailClaim?.Value;
                if (string.IsNullOrEmpty(email)) return false;
                return true;
            }else
            {
                return false;
            }
        }


    }
}
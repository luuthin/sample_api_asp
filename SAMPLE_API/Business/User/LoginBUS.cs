﻿using SAMPLE_API.Common;
using SAMPLE_API.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static SAMPLE_API.Utils.Logger;

namespace SAMPLE_API.Business.User
{
    public class LoginBUS
    {
        public static bool CheckUserLogin(string email, string password)
        {
            string DAO_Email = "admin@gmail.com";
            string DAO_Pass = "123456";

            try
            {
                // check user login
                if (email == DAO_Email && password == DAO_Pass)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("" , ex, Level.ERROR);
                throw ex;
            }
            
        }

        public static ErrorDTO CheckSignature(string timestamp, string signature)
        {

            ErrorDTO error = new ErrorDTO();

            string signatureGen = Signature.GenerateSignature(timestamp);
                // check user login
            if (signature == signatureGen)
            {
               
                return error = null;
            }
            else
            {
                error.code = Constans.INVALID_DATA_CODE;
                error.message = Constans.INVALID_DATA_MSG;

                Logger.Info(error.code + " || " + error.message + " || Timestamp :" + timestamp + " || Signature :" + signature, Level.ERROR);

                return error;
            }

        }
    }
}
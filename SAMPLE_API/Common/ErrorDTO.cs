﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAMPLE_API.Common
{
    public class ErrorDTO
    {
        public string message { get; set; }
        public string code { get; set; }
    }
}